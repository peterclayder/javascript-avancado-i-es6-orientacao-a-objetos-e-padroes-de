
// recebe os inputs
var campos = [
    document.querySelector('#data'),
    document.querySelector('#valor'),
    document.querySelector('#quantidade')
];

// recebe o tbody da tabela 
var tbody = document.querySelector('table tbody');

// realiza um evento quando, der submit no formulário
document.querySelector('.form').addEventListener('submit', function (event) {

    // Após submeter o formulário, evita que a pagina seja atualizada
    event.preventDefault();

    var tr = document.createElement("tr");

    campos.forEach(function (campo) {
        var td = document.createElement('td');
        td.textContent = campo.value;
        tr.appendChild(td);
    });

    var tdVolume = document.createElement('td');
    tdVolume.textContent = campos[1].value * campos[2].value;
    tr.appendChild(tdVolume);

    tbody.appendChild(tr);

    // limpa os campos
    campos[0].value = '';
    campos[1].value = 1;
    campos[2].value = 0;

    campos[0].focus();
});


console.log(campos);


