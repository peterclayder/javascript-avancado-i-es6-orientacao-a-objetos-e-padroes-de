class NegociacaoController {

    constructor() {

        /**
         * A variável $ vai receber a função querySelector.
         * Utilizamos o bind, para que o $ continue mantendo associação 
         * com o document
         */
        let $ = document.querySelector.bind(document);

        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');

    }

    adiciona(event) {

        event.preventDefault();

        /*
         * spread operator ( ... )
         * Adicionamos ... (reticências) posicionado antes do this, 
         * com este spread operator, indicamos que o array será desmembrado 
         */
        let data = new Date(...
                /*
                 * Transformamos a string data em um array (ano, mes, dia)
                 * [2016, 12, 13] 
                 */
                this._inputData.value
                .split('-')
                .map(function (item, indice) {
                    /*
                     * temos que diminuir 1 do mês, pois em JS a contagem do mês
                     * começa a partir do 0;
                     * O indice do mês no array é igual a 1
                     * ( item - indice % 2 ) Apenas o mod de 1 é igual a 1, ou seja 
                     * apenas quando estiver no indice 1 será diminuído 1, no restante 
                     * será diminuído 0.
                     */
                    return item - indice % 2;
                }));

        console.log(data);

        let negociacao = new Negociacao(
                data,
                this._inputQuantidade.value,
                this._inputValor.value
                );

        console.log(negociacao);
    }
}


