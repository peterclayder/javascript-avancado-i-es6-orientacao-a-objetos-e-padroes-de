
class Negociacao{
    
    constructor(date, quantidade, valor){
        this._data = new Date(date.getTime());
        this._quantidade = quantidade;
        this._valor = valor;
        this._volume = this._quantidade * this._valor;
        
        /*
         * transformo os atributos em privados 
         */
        Object.freeze(this);
    }
    
    get volume(){
        return this._volume;
    }
    
    get data(){
        return new Date(this._data.getTime());
    }
    
    get valor(){
        return this._valor;
    }
    
    get quantidade(){
        return this._quantidade;
    }
    
}